const branch = process.env.CI_COMMIT_BRANCH

const config = {
    branches: [
        { name: 'master', prerelease: false },
        { name: 'development', prerelease: true }],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/npm"
    ],
    "npmPublish": false
}

if (branch == 'master') {
    config.plugins.push('@semantic-release/gitlab', '@semantic-release/changelog', [
        '@semantic-release/git',
        {
            assets: [
                "package.json",
                "package-lock.json",
                "CHANGELOG.md"],
            message: 'chore(release): ${nextRelease.version}\n\n${nextRelease.notes}',
        },
    ])
} else if (branch == 'development') {
    config.plugins.push([
        '@semantic-release/git',
        {
            assets: [
                "package.json",
                "package-lock.json"],
            message: 'chore(prerelease): ${nextRelease.version}\n\n${nextRelease.notes}',
        },
    ])
}

module.exports = config