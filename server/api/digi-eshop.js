var express = require('express');
var router = express.Router();

const convertxml = require('xml-js');

const shop = require('./shop-common');

/**
 * @swagger
 *
 * /api/digi-eshop/{brand}/data.xml:
 *   get:
 *     summary: Export Digi-Eshop XML feed, with optional brand and EAN filtering
 *     description: Either insert brand in url as /api/digi-eshop/{brand}/data.xml or ean using /api/digi-eshop/EAN={ean}/data.xml If no brand or EAN is specified, returns unfiltered XML
 *     tags:
 *      -   Digi-Eshop
 *     parameters:
 *      -   name: brand
 *          in: path
 *          required: false
 *          type: string
 *          pattern: "^[a-zA-Z]+/"
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
router.get('/(*/)?data.xml', async (req, res, next) => {
    var brands = req.originalUrl.replace('/api/digi-eshop/', '').split('/');
    brands.pop();

    const url = 'https://www.digi-eshop.cz/universal.xml?hash=' + process.env.DIGI_ESHOP_HASH;
    shop.stripByBrandsAndEansAsync(brands, url, res, true, shop.addShippingInfo).then((data) => {
        res.set('Content-Type', 'text/xml');
        res.end(convertxml.js2xml(data, shop.convertXmlSettings));
    }).catch((err) => {
        res.logError('Failed to create feed:', err.message);
        next(err);
    });
});

/**
 * @swagger
 *
 * /api/digi-eshop/{brand}/availability.xml:
 *   get:
 *     summary: Create availability feed using XML from Premier with optional brand and EAN filtering
 *     description: Either insert brand in url as /api/digi-eshop/{brand}/availability.xml or ean using /api/digi-eshop/EAN={ean}/availability.xml If no brand or EAN is specified, returns unfiltered XML
 *     tags:
 *      -   Digi-Eshop
 *     parameters:
 *      -   name: brand
 *          in: path
 *          required: false
 *          type: string
 *          pattern: "^[a-zA-Z]+/"
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
router.get('/(*/)?availability.xml', async (req, res, next) => {
    var brands = req.originalUrl.replace('/api/digi-eshop/', '').split('/');
    brands.pop();

    const url = 'https://www.digi-eshop.cz/universal.xml?hash=' + process.env.DIGI_ESHOP_HASH;
    shop.stripByBrandsAndEansAsync(brands, url, res, false).then(async (feedData) => {
        res.set('Content-Type', 'text/xml');
        res.end(convertxml.js2xml(await shop.createAvailabilityFeed(res, feedData), shop.convertXmlSettings));
    }).catch((err) => {
        res.logError('Failed to create feed:', err.message);
        next(err);
    });
});

/**
 * @swagger
 *
 * /api/digi-eshop/{brand}/availability-shoptet.xml:
 *   get:
 *     summary: Create availability feed using XML from Premier for importing back to Shoptet with optional brand and EAN filtering
 *     description: Either insert brand in url as /api/digi-eshop/{brand}/availability-shoptet.xml or ean using /api/digi-eshop/EAN={ean}/availability-shoptet.xml If no brand or EAN is specified, returns unfiltered XML
 *     tags:
 *      -   Digi-Eshop
 *     parameters:
 *      -   name: brand
 *          in: path
 *          required: false
 *          type: string
 *          pattern: "^[a-zA-Z]+/"
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
router.get('/(*/)?availability-shoptet.xml', async (req, res, next) => {
    var brands = req.originalUrl.replace('/api/digi-eshop/', '').split('/');
    brands.pop();

    const url = 'https://www.digi-eshop.cz/universal.xml?hash=' + process.env.DIGI_ESHOP_HASH;

    try {
        var setosDataPromise = shop.getSetosFeed();
        var levenhukDataPromise = shop.getLevenhukFeed();
        var feedDataPromise = shop.stripByBrandsAndEansAsync(brands, url, res, false);

        const promises = await Promise.all([setosDataPromise, levenhukDataPromise, feedDataPromise]);
        res.logTime('Downloaded required feeds');

        var setosData = promises[0];
        var levenhukData = promises[1];
        var feedData = promises[2];

        var availabilityDataPromise = shop.createAvailabilityFeed(res, feedData, false);

        var setosIds = setosData['Shop']['Shop_item'].map(a => a.Cislo._text);
        var levenhukEans = levenhukData['DATA']['ZBOZI']['POLOZKA'].map(a => {
            if (a.EAN)
                return a.EAN._text;
        });

        let shoptetData = shop.createAvailabilityFeedShoptet(
            await availabilityDataPromise,
            res,
            (item) => {
                return !setosIds.includes(item.CODE._text) && !levenhukEans.includes(item.EAN._text);
            }
        );

        res.set('Content-Type', 'text/xml');
        res.end(convertxml.js2xml(shoptetData, shop.convertXmlSettings));
    } catch (err) {
        res.logError('Failed to create feed:', err.message);
        next(err);
    }
});

module.exports = router;