const ftp = require("basic-ftp")
const convertxml = require('xml-js');
const axios = require('axios');
const fs = require('fs');
const path = require('path');


var MemoryStream = require('memory-stream');

const convertXmlSettings = { compact: true, spaces: 4 };

async function getFeed (url, feedName = 'Source') {
    axios.defaults.timeout = process.env.FEED_TIMEOUT || 10000;
    let maxAttempts = process.env.FEED_ATTEMPTS || 3;

    for (let attempt = 0; attempt < maxAttempts; attempt++) {
        try {
            let sourceFeedData = await axios.get(url);
            return sourceFeedData;
        } catch (err) {
            if (attempt >= maxAttempts - 1) {
                throw new Error(`${feedName} feed download failed - ${err.message}`);
            }
        }
    }
}

async function xml2js (data) {
    return new Promise((resolve, reject) => {
        return resolve(convertxml.xml2js(data, convertXmlSettings));
    });
}

async function js2xml (data) {
    return new Promise((resolve, reject) => {
        return resolve(convertxml.js2xml(data, convertXmlSettings));
    });
}

async function stripByBrandsAndEansAsync (brands, url, res, download = true, modifier = (data) => data) {
    try {
        let sourceFeedDataPromise = getFeed(url);

        var eans = brands.filter(str => str.match('(EAN=)([0-9]+)'));
        brands = brands.filter(x => !eans.includes(x));
        eans = eans.map(str => str.replace('EAN=', ''));

        let sourceFeedData = (await Promise.all([sourceFeedDataPromise]))[0];
        res.logTime(`Downloaded source feed`);

        var feedData = await xml2js(sourceFeedData.data.toString());

        res.log('Total count of items in XML feed:', feedData['SHOP']['SHOPITEM'].length);

        if (brands.length > 0 || eans.length > 0) {
            feedData['SHOP']['SHOPITEM'] = feedData['SHOP']['SHOPITEM'].filter((item) => {
                if (brands.length > 0 && item['MANUFACTURER'] && brands.includes(item['MANUFACTURER']._text)) {
                    return true;
                }

                if (eans.length > 0 && item['EAN'] && eans.includes(item['EAN']._text)) {
                    return true;
                }

                return false;
            });
        }

        res.log(`Count of items after filter: ${feedData['SHOP']['SHOPITEM'].length}`);

        feedData = modifier(feedData);
        res.logTime('Modified finished');
        return feedData;
    } catch (err) {
        throw err;
    }
}

async function createAvailabilityFeed (res, feedData, download = true) {
    // prevent edge case
    if (feedData['SHOP']['SHOPITEM'] === undefined)
        feedData['SHOP']['SHOPITEM'] = [];

    const client = new ftp.Client();
    const availabilityDataBuffer = new MemoryStream();

    try {
        await client.access({
            host: process.env.FTP_HOST,
            user: process.env.FTP_USER,
            password: process.env.FTP_PASS,
            secure: false
        })
        await client.downloadTo(availabilityDataBuffer, process.env.FTP_AVAILABILITY_XML_PATH);
        res.logTime('Downloaded premier feed');

        var availabilityData = await xml2js(availabilityDataBuffer.toString());
    }
    catch (err) {
        throw new Error('Failed to download or convert XML');
    }

    try {
        var stockData = [];
        availabilityData['VFPData']['que_txt'].forEach((el) => {
            if (el.sloupec03._text) {
                stockData[parseInt(el.sloupec03._text)] = el;
            }
        });

        res.log('Total count of items in sklad.xml:', availabilityData['VFPData']['que_txt'].length);
        res.log('Count of items with EAN in sklad.xml:', Object.keys(stockData).length);
        res.log('Total count filtered feed items:', feedData['SHOP']['SHOPITEM'].length);

        var availabilityFeed = {
            _declaration: availabilityData._declaration,
            item_list: {
                item: Array()
            }
        };

        feedData['SHOP']['SHOPITEM'].forEach((shopitem) => {
            if (shopitem.EAN) {
                var availability = {
                    _attributes: {
                        id: null
                    },
                    stock_quantity: { _text: null },
                    EAN: { _text: null }
                };

                try {
                    var index = parseInt(shopitem.EAN._text);

                    if (stockData[index]) {
                        availability._attributes.id = shopitem.ITEM_ID._text;
                        availability.stock_quantity._text = parseInt(stockData[index].sloupec04._text.replace(' ', ''));
                        availability.EAN._text = shopitem.EAN._text;

                        if (availability.stock_quantity._text > 0 || !download)
                            availabilityFeed['item_list']['item'].push(availability);
                    }
                } catch (err) {
                    res.logError(err);
                    throw new Error('Premier XML probably changed format');
                }

            }
        });
    }
    catch (err) {
        throw new Error('Failed to create availability XML');
    }
    res.log('Count of items in availability.xml:', availabilityFeed['item_list']['item'].length);

    if (download) {
        availabilityFeed['item_list']['item'].forEach((i) => { delete i.EAN });
    }

    return availabilityFeed;
}

function createAvailabilityFeedShoptet (shoptetAvailabilityData, res, availabilityFilter = (availabilityItem) => true) {
    shoptetAvailabilityData['SHOP'] = {
        SHOPITEM: shoptetAvailabilityData['item_list']['item']
    }

    delete shoptetAvailabilityData['item_list'];

    if (shoptetAvailabilityData['SHOP']['SHOPITEM'] === undefined)
        shoptetAvailabilityData['SHOP']['SHOPITEM'] = Array();

    shoptetAvailabilityData['SHOP']['SHOPITEM'].forEach((element, index) => {
        var newElement = {
            CODE: { _text: null },
            STOCK: {
                AMOUNT: { _text: null }
            },
            EAN: { _text: null }
        }

        newElement.CODE._text = element._attributes.id;
        newElement.STOCK.AMOUNT._text = element.stock_quantity._text;
        newElement.EAN._text = element.EAN._text;

        shoptetAvailabilityData['SHOP']['SHOPITEM'][index] = newElement;
    });

    shoptetAvailabilityData['SHOP']['SHOPITEM'] = shoptetAvailabilityData['SHOP']['SHOPITEM'].filter(availabilityFilter);
    shoptetAvailabilityData['SHOP']['SHOPITEM'] = shoptetAvailabilityData['SHOP']['SHOPITEM'].map((item) => {
        delete item.EAN;
        return item;
    });

    res.log('Count of stock items exported for Shoptet:', shoptetAvailabilityData['SHOP']['SHOPITEM'].length);

    return shoptetAvailabilityData;
}

async function getSetosFeed () {
    const setosRequest = await getFeed('http://www.setos.cz/upload/xml/promchat83125.xml', 'Setos');

    return await xml2js(setosRequest.data.toString());
}

async function getLevenhukFeed () {
    const levenhukRequest = await getFeed('http://www.levenhukshop.cz/seznam-zbozi.aspx', 'Levenhuk');
    return await xml2js(levenhukRequest.data.toString());
}

function addShippingInfo (data, costLimit = 5000) {
    data['SHOP']['SHOPITEM'].forEach(element => {
        // Remove empty tags
        Object.keys(element).forEach(key => {
            if (!Array.isArray(element[key]) && Object.keys(element[key]).length === 0) {
                delete element[key];
            } else if (Array.isArray(element[key])) {
                element[key] = element[key].filter(arrayKeyElement => Object.keys(arrayKeyElement).length !== 0);
            }
        });

        let cod = 29;
        let shippingDpd = 129;
        let shippingZasilkovna = 99;

        if (parseInt(element['PRICE_VAT']._text) > costLimit) {
            shippingDpd = 0;
            shippingZasilkovna = 0;
        }

        // Add shipping info
        element['DELIVERY_DATE'] = { _text: '0' };
        element['DELIVERY'] = [
            {
                DELIVERY_ID: { _text: 'DPD' },
                DELIVERY_PRICE: { _text: (shippingDpd).toString() },
                DELIVERY_PRICE_COD: { _text: (shippingDpd + cod).toString() }
            }, {
                DELIVERY_ID: { _text: 'ZASILKOVNA' },
                DELIVERY_PRICE: { _text: (shippingZasilkovna).toString() },
                DELIVERY_PRICE_COD: { _text: (shippingZasilkovna + cod).toString() }
            }
        ]
    });

    return data;
}

async function exportMallCzcFeed (res, filename) {
    const client = new ftp.Client();
    const mallCzcDataBuffer = new MemoryStream();

    try {
        await client.access({
            host: process.env.FTP_HOST,
            user: process.env.FTP_USER,
            password: process.env.FTP_PASS,
            secure: false
        })

        await client.downloadTo(mallCzcDataBuffer, 'homes/sklad premier/Public feed/mallczcfeed.xml');
        res.logTime('Downloaded mallczcfeed.xml');

        var mallCzcData = mallCzcDataBuffer.toString();
    }
    catch (err) {
        console.error(err);
        throw new Error('Failed to download XML');
    }

    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };


    mallCzcData = mallCzcData.replaceAll('VFPData', 'SHOP');
    mallCzcData = mallCzcData.replaceAll('que_txt', 'SHOPITEM');

    
    mallCzcData = mallCzcData.replaceAll('sloupec01', 'ID');    
    mallCzcData = mallCzcData.replaceAll('sloupec02', 'NAME');    
    mallCzcData = mallCzcData.replaceAll('sloupec03', 'EAN');    
    mallCzcData = mallCzcData.replaceAll('sloupec04', 'STOCK');    
    mallCzcData = mallCzcData.replaceAll('sloupec05', 'PURCHASEPRICE');
    mallCzcData = mallCzcData.replaceAll('sloupec06', 'PRICE_NO_VAT');

    return mallCzcData;
}

module.exports = {
    convertXmlSettings,
    xml2js,
    js2xml,
    stripByBrandsAndEansAsync,
    createAvailabilityFeed,
    createAvailabilityFeedShoptet,
    addShippingInfo,
    getSetosFeed,
    getLevenhukFeed,
    exportMallCzcFeed
}