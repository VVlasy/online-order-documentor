var express = require('express');
var router = express.Router();

const convertxml = require('xml-js');
const shop = require('./shop-common');

async function createSetosShoptetFeed () {
    let setosData = await shop.getSetosFeed();

    var combinedData = {
        SHOP: {
            SHOPITEM: Array()
        }
    };

    setosData['Shop']['Shop_item'].forEach(item => {
        var newItem = {
            CODE: { _text: item.Cislo._text },
            NAME: { _text: item.Popis._text },
            CATEGORIES: {
                CATEGORY: { _text: 'Nezařazeno' }
            },
            IMAGES: {
                IMAGE: { _text: item.Obrazek._text }
            },
            VAT: { _text: '21' },
            AVAILABILITY_OUT_OF_STOCK: { _text: 'očekáváme naskladnění' },
            AVAILABILITY_IN_STOCK: { _text: 'SKLADEM' },
            STOCK: {
                AMOUNT: { _text: item.Dostupnost._text }
            },
            PRICE: { _text: parseInt(item.Doporucena_cena._text) }
        };

        if (item.Individualni_cena_zakaznika._text) {
            newItem['PURCHASE_PRICE'] = {};
            newItem['PURCHASE_PRICE']['_text'] = (parseInt(item.Individualni_cena_zakaznika._text) * 1.21).toFixed(2);
        }

        if (item.EAN._text) {
            newItem['EAN'] = {};
            newItem['EAN']['_text'] = item.EAN._text.substring(0, 14);
        }

        if (item.Vyrobce._text) {
            newItem['MANUFACTURER'] = {};
            newItem['MANUFACTURER']['_text'] = item.Vyrobce._text;
        }


        if (item.Kratky_popis._text) {
            newItem['SHORT_DESCRIPTION'] = {};
            newItem['SHORT_DESCRIPTION']['_text'] = item.Kratky_popis._text;
        }


        if (item.Technicke_parametry.Technicky_parametr && item.Technicke_parametry.Technicky_parametr.length) {
            item.Technicke_parametry.Technicky_parametr.forEach((techParam) => {
                var paramItem = {
                    NAME: {},
                    VALUE: {}
                };

                if (techParam.Name._text)
                    paramItem.NAME['_text'] = techParam.Name._text;

                if (techParam.Value._text)
                    paramItem.VALUE['_text'] = techParam.Value._text;

                if (techParam.Name._text && techParam.Value._text) {
                    if (!newItem.TEXT_PROPERTIES) {
                        newItem['TEXT_PROPERTIES'] = {
                            TEXT_PROPERTY: Array()
                        }
                    }

                    newItem.TEXT_PROPERTIES.TEXT_PROPERTY.push(paramItem);
                }

            });
        }

        if (!item.Popis._text.toLowerCase().includes('lowepro') &&
            !item.Popis._text.toLowerCase().includes('joby') && 
            !(item.Vyrobce._text && item.Vyrobce._text.toLowerCase().includes('lowepro')) &&
            !(item.Vyrobce._text && item.Vyrobce._text.toLowerCase().includes('joby')) && 
            !(item.Kratky_popis._text && item.Kratky_popis._text.toLowerCase().includes('lowepro')) &&
            !(item.Kratky_popis._text && item.Kratky_popis._text.toLowerCase().includes('joby')))
            combinedData['SHOP']['SHOPITEM'].push(newItem);
    });
    let result = await shop.js2xml(combinedData);

    combinedData = null;
    setosData = null;

    return result;
}

/**
 * @swagger
 *
 * /api/Setos/feed.xml:
 *   get:
 *     summary: returns pure unedited Setos XML feed
 *     tags:
 *      -   Setos
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
router.get('/feed.xml', async (req, res, next) => {
    try {
        let data = await shop.getSetosFeed();
        let xmlData = await shop.js2xml(data);

        res.set('Content-Type', 'text/xml');
        res.end(xmlData);
    } catch (err) {
        res.logError(err);
        next(err);
    }

});

/**
 * @swagger
 *
 * /api/Setos/feed-shoptet.xml:
 *   get:
 *     summary: Generates XML feed for Shoptet import
 *     tags:
 *      -   Setos
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
router.get('/feed-shoptet.xml', async (req, res, next) => {
    try {
        let data = await createSetosShoptetFeed();

        res.set('Content-Type', 'text/xml');
        res.end(data);
    } catch (err) {
        res.logError(err);
        next(err);
    }
});

module.exports = router;