var express = require('express');
var router = express.Router();

const bodyParser = require('body-parser')
const ftp = require("basic-ftp")
const Stream = require('stream');
const { exception } = require('console');
var multer = require('multer');
var upload = multer({ storage: multer.memoryStorage() });

const jsonParser = bodyParser.json({ limit: '50mb', extended: true });
const formParser = bodyParser.urlencoded({ extended: true });
const multiPartFormParser = upload.fields([{ name: 'images', maxCount: 10 }]);

/**
 * @swagger
 *
 * definitions:
 *   ImageData:
 *     type: object
 *     required:
 *       - name
 *       - image
 *     properties:
 *       name:
 *         type: string
 *         example: 78457214114
 *       image:
 *         type: blob
 *         example: 
 */

/**
 * @swagger
 *
 * /api/uploadImage:
 *   post:
 *     summary: Upload an image blob to FTP storage
 *     tags:
 *          - Documentor web APP
 *     requestBody:
 *      content:
 *          multipart/form-data:
 *              schema:
 *                  $ref: '#/definitions/ImageData'
 *     responses:
 *      200:
 *          description: file saved
 *          content: 
 *              'application/json': {}
 *      400:
 *          description: failed to convert image from base64 or failed to upload to FTP
 *          content: 
 *              'application/json': {}
 */
router.post('/', multiPartFormParser, async (req, res) => {
    const client = new ftp.Client();
    var error = 0;

    try {
        if (!('name' in req.body) || !('images' in req.files)) {
            throw new Error('Missing required parameters:' + ('name' in req.body ? '' : ' name') + ('images' in req.files ? '' : ' images'));
        }

        req.logTime('File upload to node ended at');

        //client.ftp.verbose = true
        try {
            await client.access({
                host: process.env.FTP_HOST,
                user: process.env.FTP_USER,
                password: process.env.FTP_PASS,
                secure: false
            })
            await client.ensureDir(`${process.env.FTP_PHOTOS_FOLDER_PATH}/${req.body.name}`);
            console.log(`Ensuring dir: ${process.env.FTP_PHOTOS_FOLDER_PATH}/${req.body.name}`)

            if(req.body.name && req.body.name != ""){
                console.log("Clearing working dir");
                //await client.clearWorkingDir();
            }
            
        }
        catch (err) {
            res.status(400).send({
                message: 'Failed to create directory on FTP server'
            });

            res.logError(`Failed to create directory ${process.env.FTP_PHOTOS_FOLDER_PATH}/${req.body.name} on FTP`);
            client.close();
            error++;
        }

        for (let idx = 0; idx < req.files.images.length; idx++) {
            const image = req.files.images[idx];
            var [mime, filename] = image.mimetype.split('/');

            filename = `${req.body.name}_${idx + 1}.${filename}`;
            const fullfilename = 'uploads/' + filename;

            if (mime != 'image') {
                throw new Error('Invalid file type'); // TODO: maybe check mimetypes beforehand?
            }

            try {
                req.logTime('Start image upload: ', filename, ' at ');

                const readableStream = new Stream.Readable();
                readableStream._read = () => { } // _read is required but you can noop it
                readableStream.push(image.buffer, 'ascii');
                readableStream.push(null);

                await client.uploadFrom(readableStream, filename);

                req.logTime('Uploaded image: ', filename, ' at ');
            }
            catch (err) {
                res.status(400).send({
                    message: 'Failed to upload to FTP'
                });

                res.logError(`Failed to upload file ${fullfilename} to FTP`);

                error++;
            } finally {
                if (error)
                    break;
            }

        }

        req.logTime('File upload to FTP ended at');

        if (!error) {
            res.json({ message: 'success!' });
        }
    }
    catch (err) {
        res.logError(`${err.message}`);

        if (!res.headersSent)
            res.status(400).json({ message: err.message });
    } finally {
        client.close();
    }

});

module.exports = router;