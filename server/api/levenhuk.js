var express = require('express');
var router = express.Router();

const convertxml = require('xml-js');

const shop = require('./shop-common');

async function createLevenhukShoptetFeed (priceRaise = 1) {
    var levenhukData = await shop.getLevenhukFeed();

    var combinedData = {
        SHOP: {
            SHOPITEM: Array()
        }
    };

    levenhukData['DATA']['ZBOZI']['POLOZKA'].forEach(item => {
        var newItem = {
            CODE: { _text: item.KOD._text },
            NAME: { _text: item.NAZEV._text },
            IMAGES: {
                IMAGE: { _text: item.OBRAZEK._text }
            },
            STOCK: {
                AMOUNT: { _text: item.ZASOBA._text }
            },
            AVAILABILITY_OUT_OF_STOCK: { _text: 'Vyprodáno' },
            AVAILABILITY_IN_STOCK: { _text: 'Skladem' },
            PRICE: { _text: (parseFloat(item.CENA_BEZ_DPH._text) / (1 - (priceRaise - 1))).toFixed(2) },
            PRICE_VAT: { _text: (parseFloat(item.CENA_BEZ_DPH._text) / (1 - (priceRaise - 1)) * 1.21).toFixed(2) }
        };

        if (item.EAN && item.EAN._text) {
            newItem['EAN'] = {};
            newItem['EAN']['_text'] = item.EAN._text;
        }

        if (item.POPIS && item.POPIS._text) {
            newItem['DESCRIPTION'] = {};
            newItem['DESCRIPTION']['_text'] = item.POPIS._text;
        }


        if (item.PARAMETRY.PARAMETR && item.PARAMETRY.PARAMETR.length) {
            item.PARAMETRY.PARAMETR.forEach((techParam) => {
                var paramItem = {
                    NAME: {},
                    VALUE: {}
                };

                if (techParam._attributes.TYP)
                    paramItem.NAME['_text'] = techParam._attributes.TYP;

                if (techParam._text)
                    paramItem.VALUE['_text'] = techParam._text;

                if (techParam._attributes.TYP && techParam._text) {
                    if (!newItem.TEXT_PROPERTIES) {
                        newItem['INFORMATION_PARAMETERS'] = {
                            INFORMATION_PARAMETER: Array()
                        }
                    }

                    newItem.INFORMATION_PARAMETERS.INFORMATION_PARAMETER.push(paramItem);
                }
            });
        }

        combinedData['SHOP']['SHOPITEM'].push(newItem);
    });
    
    let result = await shop.js2xml(combinedData);

    combinedData = null;
    levenhukData = null;

    return result;
}

async function modifyLevenhukFeed (priceRaise = 1) {
    var levenhukData = await shop.getLevenhukFeed();

    var edited = [];

    levenhukData['DATA']['ZBOZI']['POLOZKA'].forEach(item => {
        if (item.CENA_S_DPH)
            delete item.CENA_S_DPH;

        if (item.CENA_BEZ_DPH && parseFloat(item.CENA_BEZ_DPH._text))
            item.CENA_BEZ_DPH._text = ((parseFloat(item.CENA_BEZ_DPH._text) / (1 - (priceRaise - 1)))).toFixed(2);

        edited.push(item);
    });

    levenhukData['DATA']['ZBOZI']['POLOZKA'] = edited;

    return await shop.js2xml(levenhukData);
}

/**
 * @swagger
 *
 * /api/Levenhuk/feed.xml:
 *   get:
 *     summary: Passes on raw unmodified Levenhuk feed
 *     tags:
 *      - Levenhuk
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
router.get('/feed.xml', async (req, res, next) => {
    try {
        let data = await shop.getLevenhukFeed();
        let xmlData = await shop.js2xml(data);

        res.set('Content-Type', 'text/xml');
        res.end(xmlData);
    } catch (err) {
        res.logError(err);
        next(err);
    }
});

/**
 * @swagger
 *
 * /api/Levenhuk/feed-raised.xml:
 *   get:
 *     summary: Passes on raw Levenhuk feed with raised prices by 1.6%
 *     tags:
 *      - Levenhuk
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
router.get('/feed-raised.xml', async (req, res, next) => {
    try {
        let data = await modifyLevenhukFeed(1.016);

        res.set('Content-Type', 'text/xml');
        res.end(data);
    } catch (err) {
        res.logError(err);
        next(err);
    }
});

/**
 * @swagger
 *
 * /api/Levenhuk/feed-shoptet.xml:
 *   get:
 *     summary: Generates XML feed for Shoptet import
 *     tags:
 *      - Levenhuk
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
router.get('/feed-shoptet.xml', async (req, res, next) => {
    try {
        let data = await createLevenhukShoptetFeed();

        res.set('Content-Type', 'text/xml');
        res.end(data);
    } catch (err) {
        res.logError(err);
        next(err);
    }
});

module.exports = router;