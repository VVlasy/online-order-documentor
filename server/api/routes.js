var express = require('express'),
    router = express.Router();

let fs = require('fs');
var url = require('url');

router.get('/timeout-test', async (req, res, next) => {
    setTimeout(() => {
        res.write('Done 60s');
        res.end();
    }, 60 * 1000);
});

router.use('/instax', require('./instaxshop'));
router.use('/digi-eshop', require('./digi-eshop'));
router.use('/Levenhuk', require('./levenhuk'));
router.use('/Setos', require('./setos'));

router.use('/Premier', require('./premier'));

router.use('/uploadImage', require('./online-order-documentor'));

function fullUrl (req) {
    return url.format({
        protocol: req.protocol,
        host: req.get('host'),
        pathname: req.originalUrl
    });
}

router.use(async (err, req, res, next) => {
    if (req.is('application/json')) {
        res.status(400).json({
            message: err.message
        });
    } else {
        let file = fs.readFileSync(__dirname + '/../errors/400.html')
            .toString()
            .replace('{errortext}', err.message)
            .replace('{errortitle}', 'Došlo k chybě při zpracování požadavku')
            .replace('{urltext}', fullUrl(req));
            
        if (!res.headersSent) {
            res.writeHead(400);
        }

        res.end(file);
    }
});

module.exports = router;