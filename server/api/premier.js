var express = require('express');
var router = express.Router();

const shop = require('./shop-common');
const convertxml = require('xml-js');

/**
 * @swagger
 *
 * /api/premier/{feedname}.xml:
 *   get:
 *     summary: publish feeds from premier folder
 *     description: 
 *     tags:
 *      -   Premier
 *     parameters:
 *      -   name: brand
 *          in: path
 *          required: false
 *          type: string
 *          pattern: "^[a-zA-Z]+/"
 *     responses:
 *      200:
 *          description: Edited XML Feed
 *          content: 
 *              'application/xml': {}
 *      400:
 *          description: failed to edit XML feed
 *          content: 
 *              'application/json': {}
 */
 router.get('/*.xml', async (req, res, next) => {
    try {
        let data = await shop.exportMallCzcFeed(res, '');

        res.set('Content-Type', 'text/xml');
        res.end(data);
    } catch (err) {
        res.logError(err);
        next(err);
    }
});

module.exports = router;