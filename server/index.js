require('dotenv').config()

const express = require('express');
const path = require('path');

const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const app = express();


// Serve the static files from the React app
app.use(express.static(path.join(__dirname, '/../build')));

app.use(function (req, res, next) {
  var startTime = 0;
  var requestId = require('shortid').generate();

  req['requestId'] = requestId;
  res['requestId'] = requestId;

  req['log'] = (...text) => console.log(`[${requestId}]`, ...text);
  res['log'] = (...text) => console.log(`[${requestId}]`, ...text);

  req['logError'] = (...text) => console.error(`[${requestId}]`, ...text);
  res['logError'] = (...text) => console.error(`[${requestId}]`, ...text);


  req['logTime'] = (...text) => {
    let localTime = process.hrtime(startTime);
    console.log(`[${requestId}]`, ...text, `${localTime[0]}.${localTime[1]}s`);
  };
  res['logTime'] = (...text) => {
    let localTime = process.hrtime(startTime);
    console.log(`[${requestId}]`, ...text, `${localTime[0]}.${localTime[1]}s`);
  };

  startTime = process.hrtime();


  function afterResponse () {
    res.removeListener('finish', afterResponse);
    res.removeListener('close', afterResponse);

    // action after response
    var endTime = process.hrtime(startTime);
    res.log('Request took', `${endTime[0]}.${endTime[1]}s`, 'to complete.');
  }

  res.on('finish', afterResponse);
  res.on('close', afterResponse);

  // action before request
  // eventually calling `next()`
  res.log('Request for: ', req.protocol + '://' + req.get('host') + req.originalUrl);
  next();
});

const options = {
  definition: {
    openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
    info: {
      title: 'Online Order Documentor', // Title (required)
      version: '1.0.0', // Version (required)
    },
  },
  // Path to the API docs
  apis: ['./index.js', './**/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use('/api', require('./api/routes'));

// Handles any requests that don't match the ones above
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/../build/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log('App is listening on port ' + port);