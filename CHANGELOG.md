## [1.5.1](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.5.0...v1.5.1) (2021-04-26)


### Bug Fixes

* **premier:** add solution for heroku ([99b97da](https://gitlab.com/VVlasy/online-order-documentor/commit/99b97da30ecab14728f80e4d2d98f53d97b44f4f))

# [1.5.0](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.16...v1.5.0) (2021-04-26)


### Features

* **premier:** add premier publish ([08768b0](https://gitlab.com/VVlasy/online-order-documentor/commit/08768b010b38a8feef449e3e2d624c8b66860a82))

## [1.4.16](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.15...v1.4.16) (2021-04-07)


### Bug Fixes

* add filter for joby and lowepro ([a254bf3](https://gitlab.com/VVlasy/online-order-documentor/commit/a254bf33995b11544b0f41fb1d44ec2d694120c4))

## [1.4.15](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.14...v1.4.15) (2021-04-07)


### Bug Fixes

* dont clear working dir ([23f2f21](https://gitlab.com/VVlasy/online-order-documentor/commit/23f2f21ad7788febcc52dbe58bd04629aa64e509))
* **feed:** cleanup ([4e3272c](https://gitlab.com/VVlasy/online-order-documentor/commit/4e3272c639e4d3635f794e23e088795ada87f404))
* **feed:** enable caching of remote feeds ([ac56184](https://gitlab.com/VVlasy/online-order-documentor/commit/ac56184078b7b21724a2027db871897ceec3cfe9))
* **feed:** fix cache memory leak ([4f46531](https://gitlab.com/VVlasy/online-order-documentor/commit/4f46531d6bb529d53ab0cec03219eb5b81afecd3))
* **server:** remove extend timeout middleware ([5f5187b](https://gitlab.com/VVlasy/online-order-documentor/commit/5f5187bac174c9cfbb84ec78a05904125f9b7143))

## [1.4.14](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.13...v1.4.14) (2021-03-02)


### Bug Fixes

* **feed:** adapt endpoints for heroku timeout workaround ([c3b4f25](https://gitlab.com/VVlasy/online-order-documentor/commit/c3b4f2529d42b34c5ec22c8b6cac5f4a1e4c0993))
* **test:** Add long running endpoint to test heroku timeout workaround ([d58c0a9](https://gitlab.com/VVlasy/online-order-documentor/commit/d58c0a9c3181bee97c41404c3b80fe07ea693c3c))

## [1.4.13](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.12...v1.4.13) (2021-02-18)


### Bug Fixes

* **feed:** try fix memory leak? ([ea471ca](https://gitlab.com/VVlasy/online-order-documentor/commit/ea471ca17cb29272217eba07bd6ea5c2f4067031))

## [1.4.12](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.11...v1.4.12) (2021-02-18)


### Bug Fixes

* **feed:** speed microoptimization ([77ede73](https://gitlab.com/VVlasy/online-order-documentor/commit/77ede737dd8013d5f5f015ad14bb56a3bed154ee))

## [1.4.11](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.10...v1.4.11) (2021-02-16)


### Bug Fixes

* **feed:** when getting multiple feeds do it async ([465feba](https://gitlab.com/VVlasy/online-order-documentor/commit/465febaaf9cd3fbee9d7e6199d3761f14865292a))

## [1.4.10](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.9...v1.4.10) (2021-02-16)


### Bug Fixes

* **server:** add error page and handler ([fd55efd](https://gitlab.com/VVlasy/online-order-documentor/commit/fd55efdf2cab6ca5fccb2bd8e2e0ccd8002ea589))

## [1.4.9](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.8...v1.4.9) (2021-02-16)


### Bug Fixes

* **feed:** apply timeout to all feed downloads ([88265e8](https://gitlab.com/VVlasy/online-order-documentor/commit/88265e814b9e67a3b56ffba2329fc60b9e4d9d0e))

## [1.4.8](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.7...v1.4.8) (2021-02-16)


### Bug Fixes

* **feed:** add timeout for dowloading source feed ([193ffc3](https://gitlab.com/VVlasy/online-order-documentor/commit/193ffc3a88ecde3680602c082b3fc5f4c502646d))
* **logging:** logtime showing up as an error log ([52af23b](https://gitlab.com/VVlasy/online-order-documentor/commit/52af23bf30e0668e7e35c59f806a2a341249c8af))
* **server:** allow feed timeout to be set from env ([2c31e41](https://gitlab.com/VVlasy/online-order-documentor/commit/2c31e4144d1c0c598873cf1e78c3a53893bb3218))
* **server:** proper throwing and handling of errors ([9cae120](https://gitlab.com/VVlasy/online-order-documentor/commit/9cae120b24a12e349a2b8f85762b144a1fa5e840))
* **server:** remove unused reference ([b2cb4bf](https://gitlab.com/VVlasy/online-order-documentor/commit/b2cb4bfef29c7a0c1096c9b4ef8a02b6a951a995))

## [1.4.7](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.6...v1.4.7) (2021-02-15)


### Bug Fixes

* **feed:** wrong filter behavior ([7c279f2](https://gitlab.com/VVlasy/online-order-documentor/commit/7c279f204bf35c6955fe4bce14a249beb0861b85))

## [1.4.6](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.5...v1.4.6) (2021-02-13)


### Bug Fixes

* **feed:** remove redundant conversions between xml and json ([83092c7](https://gitlab.com/VVlasy/online-order-documentor/commit/83092c70329452750097a7ff5ffe1259ee62d9c0))
* **log:** loging cleanup and error handling ([4e9ea64](https://gitlab.com/VVlasy/online-order-documentor/commit/4e9ea6438662bfed1d0c7436d3224f46f38b3fd4))

## [1.4.5](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.4...v1.4.5) (2021-02-13)


### Bug Fixes

* **feed:** speed up filtering by Ean and Brand ([24c2caf](https://gitlab.com/VVlasy/online-order-documentor/commit/24c2caf595a17cdb1bc2c972f4b7c2ff2a2c7523))

## [1.4.4](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.3...v1.4.4) (2021-02-13)


### Bug Fixes

* **feed:** availability feeds in memory ([291bbd3](https://gitlab.com/VVlasy/online-order-documentor/commit/291bbd3845f50843e52350274985e79564fda22a))
* **feed:** fix endpoints to reflect changes to in memory feed edits ([0d4f9b2](https://gitlab.com/VVlasy/online-order-documentor/commit/0d4f9b22c3e3056fd570042523f79575227ece78))
* **feeds:** improve perfomance by doing operations in memory ([b0bb53e](https://gitlab.com/VVlasy/online-order-documentor/commit/b0bb53e56423ba8f697ba919bc48910f41ae453f))
* **feeds:** modify stripByBrandsAndEans to be in memory ([7016d03](https://gitlab.com/VVlasy/online-order-documentor/commit/7016d030f1acbbd2a7948ce93762846c1297a8a6))
* **npm:** packages cleanup ([0a192d9](https://gitlab.com/VVlasy/online-order-documentor/commit/0a192d96962ddf36abbf8e79530348cb7975c5c2))

## [1.4.3](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.2...v1.4.3) (2021-02-12)


### Bug Fixes

* **feed:** remove appending of levenhuk to item code ([7554b8a](https://gitlab.com/VVlasy/online-order-documentor/commit/7554b8aba0121a685ffcdaa138752e70b813dd0c))
* **feeds:** modify feeds as described by customer ([a87199e](https://gitlab.com/VVlasy/online-order-documentor/commit/a87199e03dcc3c935ac9a319b38c1b9c20958e2f))

## [1.4.2](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.1...v1.4.2) (2021-02-10)


### Reverts

* Revert "fix(feed): correct Levenhuk feed format for shoptet" ([225a8fe](https://gitlab.com/VVlasy/online-order-documentor/commit/225a8fe824b47369e987929b43cce66813c26114))

## [1.4.1](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.4.0...v1.4.1) (2021-02-09)


### Bug Fixes

* **app:** debug things ([1e34da0](https://gitlab.com/VVlasy/online-order-documentor/commit/1e34da019c4311da9b55783ec1c89b6f777a0471))
* **feed:** correct Levenhuk feed format for shoptet ([aa05178](https://gitlab.com/VVlasy/online-order-documentor/commit/aa051785e4352c20f6c5b6f20a7446929cdb47bc))
* **feeds:** correct url for Levenhuk ([a60386a](https://gitlab.com/VVlasy/online-order-documentor/commit/a60386a9e13aaa1fb69a42cb1897d97caf844117))
* **ftp:** close ftp after closing http ([67e523f](https://gitlab.com/VVlasy/online-order-documentor/commit/67e523f33402153e56443386c294eee236f8829a))
* **FTP:** change call order ([a301565](https://gitlab.com/VVlasy/online-order-documentor/commit/a3015658cef0b13f0ef867568dba6f2b7903c497))
* **FTP:** dont close before all files are uploaded ([d887225](https://gitlab.com/VVlasy/online-order-documentor/commit/d8872256e9c450c2537e68df47ff5e6ed0eb7de3))
* **FTP:** remove redundant calls ([9cfbd7d](https://gitlab.com/VVlasy/online-order-documentor/commit/9cfbd7d7f2e5b675c84fcc5754290a7f6b46020f))
* **iOS:** remove all references to cache.manifest ([c53657a](https://gitlab.com/VVlasy/online-order-documentor/commit/c53657ad1c78bf29e7037c581f94608888912d1e))
* **iOS:** remove cache ([76514a7](https://gitlab.com/VVlasy/online-order-documentor/commit/76514a7b3c523b85a67fdfef039a613dbc1ac6a8))
* **server:** add time debug to upload file ([b9524d0](https://gitlab.com/VVlasy/online-order-documentor/commit/b9524d078b93632d45b6510d0839c3c8c7d50f74))
* **server:** do all image upload operations in memory ([faba2f7](https://gitlab.com/VVlasy/online-order-documentor/commit/faba2f781b9a53956e4378f9d81af9ac9c4610da))

# [1.4.0](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.3.0...v1.4.0) (2021-02-09)


### Bug Fixes

* **CI:** auto publish on release ([ee5822a](https://gitlab.com/VVlasy/online-order-documentor/commit/ee5822ab4590e1158d82677514928af44cd5770d))
* **CI:** only manual deploy ([a53dbf5](https://gitlab.com/VVlasy/online-order-documentor/commit/a53dbf575cfa430b2945f221081707d3422e32c1))
* **CI:** test ([e687db5](https://gitlab.com/VVlasy/online-order-documentor/commit/e687db57f8a0b1f1ff33a90c0d81fe89eb457251))
* **CI:** when condition ([d2bd6a4](https://gitlab.com/VVlasy/online-order-documentor/commit/d2bd6a425d65d61f152e3ff17a17ab66c449ec8a))
* abandon use of settimeout for animations ([5f1e6c6](https://gitlab.com/VVlasy/online-order-documentor/commit/5f1e6c6f5a57aa6ee45e4b8aacbb73201f4f9d56))
* add ios caching ([dbf3dfc](https://gitlab.com/VVlasy/online-order-documentor/commit/dbf3dfceb863510684e8ff900471bef6ebe3735a))
* animations when moving between screens ([8f4c47c](https://gitlab.com/VVlasy/online-order-documentor/commit/8f4c47c3a6d9eacf249499b4c9cc397f6758a044))
* bad rendering in safario ios browser (not standalone) ([1eb1838](https://gitlab.com/VVlasy/online-order-documentor/commit/1eb1838783375f7a341a3982244e07b29bae3f5b))
* bad rendering in safario ios browser (not standalone) ([3071eb2](https://gitlab.com/VVlasy/online-order-documentor/commit/3071eb20308783e8e21a9fe9c1242319562963e3))
* bad rendering on older android ([ba65d9c](https://gitlab.com/VVlasy/online-order-documentor/commit/ba65d9cf18b73d77ae7f492bec4506985873a743))
* CI to push to heroku ([3df0d03](https://gitlab.com/VVlasy/online-order-documentor/commit/3df0d0397931c3a2d8c3407e9457801a1e2bbf0f))
* **loader:** localization for multiples or singles photos ([935220a](https://gitlab.com/VVlasy/online-order-documentor/commit/935220aff84fb539bde2915d0535dd97b4ad3fe9))
* correct PWA name ([fb4c89d](https://gitlab.com/VVlasy/online-order-documentor/commit/fb4c89d1a66acc79302f231402f790bef0749403))
* crash on multiple camera reopens on iOS devices ([f6bcccb](https://gitlab.com/VVlasy/online-order-documentor/commit/f6bcccb4f39f5640dc3594f6ea389b503f1a8a69))
* enable camera button only when camera ready ([63d3a4a](https://gitlab.com/VVlasy/online-order-documentor/commit/63d3a4a2c1af84bcfb6bcd7626fbee20c88c0532))
* improve error handling for missing source feeds ([8ef661e](https://gitlab.com/VVlasy/online-order-documentor/commit/8ef661eef5b87bfae6faefc8f6dfb549254732a8))
* prevent ios no home button obscuring notification ([0a9b84a](https://gitlab.com/VVlasy/online-order-documentor/commit/0a9b84aab665bc5444b83710e36b96054a99f2d3))
* prevent taking photo before camera is ready ([a735a65](https://gitlab.com/VVlasy/online-order-documentor/commit/a735a65a93ae8c085db45533906335af30836437))
* webkit css fixes ([dfef532](https://gitlab.com/VVlasy/online-order-documentor/commit/dfef532f0632976852a5c7dcb6f57c4f075eab54))


### Features

* upload result notification instead of alert ([44fd71c](https://gitlab.com/VVlasy/online-order-documentor/commit/44fd71c8b457e7dc22cc4c0753c37f4ad1e1cbbb))

# [1.3.0](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.2.0...v1.3.0) (2021-02-01)


### Bug Fixes

* ios camera safari reload ([115f2cb](https://gitlab.com/VVlasy/online-order-documentor/commit/115f2cbd354bc25b2abe6c4c31b47f4ecf0e469d))


### Features

* upload using multipart/form-data ([1574cfb](https://gitlab.com/VVlasy/online-order-documentor/commit/1574cfbe31cdba19f91d00d3a8df20531ae8869b))

# [1.2.0](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.1.2...v1.2.0) (2021-01-27)


### Features

* handle shipping cost over 5k to be free ([4835038](https://gitlab.com/VVlasy/online-order-documentor/commit/483503832c6f442e81a9f4434f5e14f2615ff2e0))

## [1.1.2](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.1.1...v1.1.2) (2021-01-27)


### Bug Fixes

* barcode scanner css responsive ([6ce30a9](https://gitlab.com/VVlasy/online-order-documentor/commit/6ce30a9ad0717de048aa2a2f4e58d0da5d0f9c33))
* create property to store placeholder barcode text ([b863d2d](https://gitlab.com/VVlasy/online-order-documentor/commit/b863d2d90df18d734af393ef7ccaa34f5ffb5679))
* images size in weird display scenarios ([06c8ffe](https://gitlab.com/VVlasy/online-order-documentor/commit/06c8ffe63b432e6ae813dc9ed8822be8cc839c87))
* switch to universal xml from heureka xml ([60fd6bd](https://gitlab.com/VVlasy/online-order-documentor/commit/60fd6bdd3a3d3923efc01774b7e50d3a21f5c7a6))
* workaround for internal quagga error ([5864044](https://gitlab.com/VVlasy/online-order-documentor/commit/586404444a4e645268228e8d7ef48822dbd8830f))

## [1.1.1](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.1.0...v1.1.1) (2021-01-23)


### Bug Fixes

* correct quagga deinitialization ([1d4e94c](https://gitlab.com/VVlasy/online-order-documentor/commit/1d4e94cc517cc78588b4ed2c64982911c632b449))
* photo buttons in landscape ([9cc07eb](https://gitlab.com/VVlasy/online-order-documentor/commit/9cc07ebb428fc25c70349c37d0bbf89477d1faa8))
* Update package.json version on CI release ([5554914](https://gitlab.com/VVlasy/online-order-documentor/commit/5554914eb0319bab9dc76c4ee237dd8534eb4645))

# [1.1.0](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.0.1...v1.1.0) (2021-01-22)


### Bug Fixes

* improve CSS ([c7a35c6](https://gitlab.com/VVlasy/online-order-documentor/commit/c7a35c6eeff1f164712352b922615a947386a700))
* improve error handler for orientation change ([6366e4b](https://gitlab.com/VVlasy/online-order-documentor/commit/6366e4b71ae56f6ae6e9e3533ea79438e71434a3))


### Features

* Improve Ui ([a23fff7](https://gitlab.com/VVlasy/online-order-documentor/commit/a23fff7ae3cde27ae0bbcda0bbf7cf2679b94d88))

## [1.0.1](https://gitlab.com/VVlasy/online-order-documentor/compare/v1.0.0...v1.0.1) (2021-01-16)


### Bug Fixes

* wrong start state ([5b07d5e](https://gitlab.com/VVlasy/online-order-documentor/commit/5b07d5e7bc700af60210cd8ab84aff16f0d6bab0))

# 1.0.0 (2021-01-16)


### Bug Fixes

* cleanup css ([23793fe](https://gitlab.com/VVlasy/online-order-documentor/commit/23793fe9b04da0f053ce674e368a52b72f492f2d))
* componentize more parts of the code ([c573a1c](https://gitlab.com/VVlasy/online-order-documentor/commit/c573a1c7362b9e2db0a59dc250a88faa467c3ca1))
* prevent white space on scroll in ios ([e5e5e29](https://gitlab.com/VVlasy/online-order-documentor/commit/e5e5e294d0846106809f583949ec611f8739a47a))
* Split photo taker into separate component ([c694734](https://gitlab.com/VVlasy/online-order-documentor/commit/c69473470275a30d0aee35f4d30065bd279e3df5))


### Features

* enable working in landscape ([1cf2ba8](https://gitlab.com/VVlasy/online-order-documentor/commit/1cf2ba82ad9d6006f3a718bfc9ab0d7ed778f9b0))
